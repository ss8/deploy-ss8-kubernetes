#Setup vCenter Provider and login

provider "vsphere" {
  user		 = "terraform@ss8lab.local"
  password	 = "Ss8TeraForm!"
  vsphere_server = "vcenter.ss8lab.local"

  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name 		= "SS8-LAB"
}

data "vsphere_datastore" "datastore" {
  name 		= "Dysfunction- VM Datastore"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_compute_cluster" "cluster" {
  name 		= "XCIPIO"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name 		= var.SUB_NET
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name 		= "ss8core-spl-8.4.1.2.0-2022-01-20_12_00_00"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}



## This is where the VM will be created

resource "vsphere_virtual_machine" "vm1" {
  name 		   = "clustername-master"
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id	   = "${data.vsphere_datastore.datastore.id}"
  wait_for_guest_ip_timeout = 0
  wait_for_guest_net_timeout = 0

  num_cpus = 5
  memory   = 16384
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "e1000"
  } 

  disk {
    label            = "disk0"
    size             = 90
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }


  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "k8s-master"
	domain    = "localhost.localdomain"

      }

      network_interface {
        ipv4_address = var.IP_1
        ipv4_netmask = 24
	
      }
        ipv4_gateway = var.gateway
      

    }
  }



}

resource "vsphere_virtual_machine" "vm2" {
  name 		   = "clustername-node1"
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id	   = "${data.vsphere_datastore.datastore.id}"
  wait_for_guest_ip_timeout = 0
  wait_for_guest_net_timeout = 0

  num_cpus = 6
  memory   = 16384
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "e1000"
  } 

  disk {
    label            = "disk"
    size             = 120
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "k8s-node1"
	domain    = "localhost.localdomain"
      }

      network_interface {
        ipv4_address = var.IP_2
        ipv4_netmask = 24
	
      }
      ipv4_gateway = var.gateway

    }
  }

}

resource "vsphere_virtual_machine" "vm3" {
  name 		   = "clustername-node2"
  resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
  datastore_id	   = "${data.vsphere_datastore.datastore.id}"
  wait_for_guest_ip_timeout = 0
  wait_for_guest_net_timeout = 0

  num_cpus = 6
  memory   = 16384
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
    adapter_type = "e1000"
  } 

  disk {
    label            = "disk"
    size             = 120
    eagerly_scrub    = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "k8s-node2"
	domain    = "localhost.localdomain"
      }

      network_interface {
        ipv4_address = var.IP_3
        ipv4_netmask = 24
	
      }
      ipv4_gateway = var.gateway

    }
  }

}