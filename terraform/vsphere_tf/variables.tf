variable "SUB_NET" {
  description = "vSphere Subnet"
  type        = string
  default     = "subnet"
}

variable "IP_1" {
  description = "Master ip"
  type        = string
  default     = "ipmaster"
}

variable "IP_2" {
  description = "Node1 ip"  
  type = string
  default     = "ipnode1"
}

variable "IP_3" {
  description = "Node2 ip"
  type = string
  default     = "ipnode2"
}

variable "gateway" {
  description = "Gateway"
  type = string
  default     = "GTW"
}

variable "clusterName" {
  description = "Cluster Name"
  type = string
  default     = "clustername "
}
